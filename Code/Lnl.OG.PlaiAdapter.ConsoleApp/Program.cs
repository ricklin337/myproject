﻿using System;

namespace Lnl.OG.PlaiAdapter.ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var serverManager = new PlaiAdapterManager();
                serverManager.Start();

                Console.WriteLine("Started PLAI Adapter Service. Press ENTER to quit...");
                Console.ReadLine();

                serverManager.Stop();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.ReadLine();
            }
        }
    }
}