﻿using System;
using System.Configuration;

namespace Lnl.OG.PlaiAdapter
{
    /// <summary>
    /// Utility class  to retrieve settings from app.config
    /// </summary>
    public class AppConfigUtility
    {
        private const string PlaiAdapterUrlKey = "PlaiAdapterUrl";
        private const string PlaiAdapterDefaultUrl = "http://*:8081/";
        private const string OpenAccessProxyUrlKey = "OpenAccessProxyUrl";
        private const string OpenAccessProxyDefaultUrl = "https://localhost:8080/api/access/onguard/openaccess/";
        private const string OpenAccessEventBridgeUriKey = "OpenAccessEventBridgeUri";
        private const string OpenAccessEventBridgeDefaultUri = "https://localhost:8080/api/access/onguard/openaccess/eventbridge";
        private const string OpenAccessApplicationIdKey = "OpenAccessApplicationId";
        private const string OpenAccessUsernameKey = "OpenAccessUsername";
        private const string OpenAccessPasswordKey = "OpenAccessPassword";
        private const string OpenAccessDirectoryIdKey = "OpenAccessDirectoryId";
        private const string OpenAccessDefaultApplicationId = "OPEN_ACCESS_NON_PRODUCTION";
        private const string OpenAccessDefaultUsername = "sa";
        private const string OpenAccessDefaultPassword = "sa";
        private const string OpenAccessDefaultDirectoryId = "id-1";
        private const string SystemGuidKey = "SystemGuid";
        private const string OnGuardDatabaseConnectionStringKey = "OnGuardDatabaseConnectionString";

        /// <summary>
        /// Gets the setting from app.config for the specified key. Returns the default value if the setting is not specified.
        /// </summary>
        /// <param name="key">The key of the application setting</param>
        /// <param name="defaultValue">The default value of the application setting</param>
        /// <returns></returns>
        public static string AppSettingOrDefault(string key, string defaultValue)
        {
            var appSetting = ConfigurationManager.AppSettings[key];
            if (string.IsNullOrEmpty(appSetting)) appSetting = defaultValue;
            return appSetting;
        }

        /// <summary>
        /// Gets the PLAI adapter REST service URL.
        /// </summary>
        /// <returns></returns>
        public static string GetPlaiAdapterUrl()
        {
            return AppSettingOrDefault(PlaiAdapterUrlKey, PlaiAdapterDefaultUrl);
        }

        /// <summary>
        /// Gets the Open Access Proxy URL.
        /// </summary>
        /// <returns></returns>
        public static string GetOpenAccessProxyUrl()
        {
            return AppSettingOrDefault(OpenAccessProxyUrlKey, OpenAccessProxyDefaultUrl);
        }

        /// <summary>
        /// Gets the Open Access Event Bridge URL.
        /// </summary>
        /// <returns></returns>
        public static string GetOpenAccessEventBridgeUrl()
        {
            return AppSettingOrDefault(OpenAccessEventBridgeUriKey, OpenAccessEventBridgeDefaultUri);
        }

        /// <summary>
        /// Gets the Open Access application id.
        /// </summary>
        /// <returns></returns>
        public static string GetOpenAccessApplicationId()
        {
            return AppSettingOrDefault(OpenAccessApplicationIdKey, OpenAccessDefaultApplicationId);
        }

        /// <summary>
        /// Gets the Open Access user name to authenticate.
        /// </summary>
        /// <returns></returns>
        public static string GetOpenAccessUsername()
        {
            return AppSettingOrDefault(OpenAccessUsernameKey, OpenAccessDefaultUsername);
        }

        /// <summary>
        /// Gets the Open Access password to authenticate.
        /// </summary>
        /// <returns></returns>
        public static string GetOpenAccessPassword()
        {
            return AppSettingOrDefault(OpenAccessPasswordKey, OpenAccessDefaultPassword);
        }

        /// <summary>
        /// Gets the Open Access directory id to authenticate.
        /// </summary>
        /// <returns></returns>
        public static string GetOpenAccessDirectoryId()
        {
            return AppSettingOrDefault(OpenAccessDirectoryIdKey, OpenAccessDefaultDirectoryId);
        }

        /// <summary>
        /// Gets the OnGuard database connection string
        /// </summary>
        /// <returns></returns>
        public static string GetOnGuardDatabaseConnectionString()
        {
            return ConfigurationManager.AppSettings.Get(OnGuardDatabaseConnectionStringKey);
        }

        /// <summary>
        /// Gets the PLAI system guid
        /// </summary>
        /// <returns></returns>
        public static string GetSystemGuid()
        {
            var systemGuid = ConfigurationManager.AppSettings[SystemGuidKey];

            if (string.IsNullOrEmpty(systemGuid))
            {
                systemGuid = Guid.NewGuid().ToStringWithCurlyBraces();
                var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                config.AppSettings.Settings.Add(SystemGuidKey, systemGuid);
                config.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection("appSettings");
            }

            return systemGuid;
        }
    }
}