﻿using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Lnl.OG.PlaiAdapter
{
    /// <summary>
    /// Represents an error from the PLAI adapter
    /// </summary>
    public class PlaiAdapterException : HttpResponseException
    {
        /// <summary>
        /// Creates a PlaiAdapterException with the HTTP response message.
        /// </summary>
        /// <param name="response">The HTTP response message</param>
        public PlaiAdapterException(HttpResponseMessage response) :
            base(response)
        {
        }

        /// <summary>
        /// Creates a PlaiAdapterException with the reason phrase and error message.
        /// </summary>
        /// <param name="reasonPhrase">The reason phrase</param>
        /// <param name="message">The error message</param>
        public PlaiAdapterException(string reasonPhrase, string message) :
            this(new HttpResponseMessage(HttpStatusCode.InternalServerError) { ReasonPhrase = reasonPhrase, Content = new StringContent(message) })
        {
        }

        /// <summary>
        /// Creates a PlaiAdapterException with the HTTP status code, reason phrase and error message.
        /// </summary>
        /// <param name="statusCode">The HTTP status code</param>
        /// <param name="reasonPhrase">The reason phrase</param>
        /// <param name="message">The error message</param>
        public PlaiAdapterException(HttpStatusCode statusCode, string reasonPhrase, string message) :
           this(new HttpResponseMessage(statusCode) { ReasonPhrase = reasonPhrase, Content = new StringContent(message) })
        {
        }

        /// <summary>
        /// Creates a PlaiAdapterException with the HTTP status code and error message.
        /// </summary>
        /// <param name="statusCode">The HTTP status code</param>
        /// <param name="errorMessage">The error message</param>
        public PlaiAdapterException(HttpStatusCode statusCode, string errorMessage) :
           this(new HttpResponseMessage(statusCode) { ReasonPhrase = errorMessage, Content = new StringContent(errorMessage) })
        {
        }
    }
}