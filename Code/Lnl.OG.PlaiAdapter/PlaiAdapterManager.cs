﻿using Lnl.OG.PlaiAdapter.OpenAccess;
using Microsoft.Owin.Hosting;
using Owin;
using System;
using System.Web.Http;

namespace Lnl.OG.PlaiAdapter
{
    /// <summary>
    /// The manager of PLAI adapter REST service.
    /// </summary>
    public class PlaiAdapterManager
    {
        private const string PlaiApiRouteTemplate = "PSIA/{controller}/{action}/{parameter}";
        private const string PlaiAreaControlApiRouteTemplate = "PSIA/AreaControl/{controller}/{action}/{parameter}";

        private IDisposable plaiAdapterRestService;
        private OpenAccessService openAccessService;
        private OpenAccessEventListener eventListener;

        /// <summary>
        /// Start PLAI adapter REST service.
        /// </summary>
        public void Start()
        {
            var plaiAdapterUrl = AppConfigUtility.GetPlaiAdapterUrl();
            plaiAdapterRestService = WebApp.Start(plaiAdapterUrl, ConfigureWebApp);

            openAccessService = new OpenAccessService();
            eventListener = new OpenAccessEventListener(openAccessService);
            eventListener.StartListening();
        }

        /// <summary>
        /// Stop PLAI adapter REST service.
        /// </summary>
        public void Stop()
        {
            eventListener.StopListening();
            openAccessService.LogOut();
            plaiAdapterRestService.Dispose();
        }

        /// <summary>
        /// Configures the PLAI adapter REST service.
        /// </summary>
        /// <param name="appBuilder">The app builder</param>
        private void ConfigureWebApp(IAppBuilder appBuilder)
        {
            var config = new HttpConfiguration();
            config.Formatters.Clear();
            config.Formatters.Add(new CustomMediaTypeFormatter());
            config.Routes.MapHttpRoute("DefaultApi", PlaiApiRouteTemplate, new { action = RouteParameter.Optional, parameter = RouteParameter.Optional });
            config.Routes.Add("AreaControlApi", config.Routes.CreateRoute(PlaiAreaControlApiRouteTemplate, new { action = RouteParameter.Optional, parameter = RouteParameter.Optional }, null));
            appBuilder.UseWebApi(config);
        }
    }
}