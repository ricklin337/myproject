﻿using System;

namespace Lnl.OG.PlaiAdapter
{
    public static class GuidExtension
    {
        /// <summary>
        /// Converts the guid to string with curly braces.
        /// </summary>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string ToStringWithCurlyBraces(this Guid guid)
        {
            return "{" + Convert.ToString(guid) + "}";
        }
    }
}