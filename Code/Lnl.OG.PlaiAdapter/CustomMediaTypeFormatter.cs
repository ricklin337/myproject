﻿using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Lnl.OG.PlaiAdapter
{
    /// <summary>
    /// The custom formatter used to remove the BOM and add namespace "urn:psialliance-org"
    /// </summary>
    public class CustomMediaTypeFormatter : BufferedMediaTypeFormatter
    {
        public CustomMediaTypeFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/xml"));
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/xml"));
        }

        public override bool CanReadType(Type type)
        {
            return true;
        }

        public override bool CanWriteType(Type type)
        {
            return true;
        }

        /// <summary>
        /// Removes the BOM and add namespace "urn:psialliance-org"
        /// </summary>
        /// <param name="type"></param>
        /// <param name="value"></param>
        /// <param name="writeStream"></param>
        /// <param name="content"></param>
        public override void WriteToStream(Type type, object value, Stream writeStream, HttpContent content)
        {
            var utf8EncodingWithNoByteOrderMark = new UTF8Encoding(false);
            using (var writer = new XmlTextWriter(writeStream, utf8EncodingWithNoByteOrderMark))
            {
                var namespaces = new XmlSerializerNamespaces();
                namespaces.Add(string.Empty, PlaiServiceNamespace);
                var serializer = new XmlSerializer(type, PlaiServiceNamespace);
                serializer.Serialize(writer, value, namespaces);
            }
        }

        public override object ReadFromStream(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var serializer = new XmlSerializer(type);
            return serializer.Deserialize(readStream);
        }

        private const string PlaiServiceNamespace = "urn:psialliance-org";
    }
}
