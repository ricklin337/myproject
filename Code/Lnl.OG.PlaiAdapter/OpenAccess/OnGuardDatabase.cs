﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Currently the Open Access does not store a unique third party ID (GUID) against credential holder (Lnl_Cardholder), credential (Lnl_Badge) and role (Lnl_AccessLevel).
    /// So this class is used to manage the GUID for the those types.
    /// TODO: Remove this file and update class OpenAccessService after OpenAccess is updated to support GUID.
    /// </summary>
    public class OnGuardDatabase
    {
        private static bool isDatabaseInitialized;

        public OnGuardDatabase()
        {
            InitializeDatabase();
        }
        
        public void UpdateAccessLevelGuid(int accessLeveId, Guid accessLevelGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("UPDATE ACCESSLVL SET OBJECT_GUID='{0}' WHERE ACCESSLVID={1}", accessLevelGuid, accessLeveId);
                sqlHelper.ExecuteNonQuery(sql);
            }
        }

        public void UpdateCardholderGuid(int cardholderId, Guid cardholderGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("UPDATE EMP SET OBJECT_GUID='{0}' WHERE ID={1}", cardholderGuid, cardholderId);
                sqlHelper.ExecuteNonQuery(sql);
            }
        }

        public void UpdateBadgeGuid(int badgeKey, Guid badgeGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("UPDATE BADGE SET OBJECT_GUID='{0}' WHERE BADGEKEY={1}", badgeGuid, badgeKey);
                sqlHelper.ExecuteNonQuery(sql);
            }
        }

        public bool IsAccessLevelExist(Guid accessLevelGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT COUNT(*) FROM ACCESSLVL WHERE OBJECT_GUID='{0}'", accessLevelGuid);
                return sqlHelper.ExecuteInt32(sql) > 0;
            }
        }

        public bool IsCardholderExist(Guid cardholderGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT COUNT(*) FROM EMP WHERE OBJECT_GUID='{0}'", cardholderGuid);
                return sqlHelper.ExecuteInt32(sql) > 0;
            }
        }

        public bool IsBadgeExist(Guid badgeGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT COUNT(*) FROM BADGE WHERE OBJECT_GUID='{0}'", badgeGuid);
                return sqlHelper.ExecuteInt32(sql) > 0;
            }
        }

        public Guid GetAccessLevelGuid(int accessLeveId)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT OBJECT_GUID FROM ACCESSLVL WHERE ACCESSLVID={0}", accessLeveId);
                return sqlHelper.ExecuteGuid(sql);
            }
        }

        public Guid GetCardholderGuid(int cardholderId)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT OBJECT_GUID FROM EMP WHERE ID={0}", cardholderId);
                return sqlHelper.ExecuteGuid(sql);
            }
        }

        public Guid GetBadgeGuid(int badgeKey)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT OBJECT_GUID FROM BADGE WHERE BADGEKEY={0}", badgeKey);
                return sqlHelper.ExecuteGuid(sql);
            }
        }

        public int GetAccessLevelId(Guid accessLeveGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT ACCESSLVID FROM ACCESSLVL WHERE OBJECT_GUID='{0}'", accessLeveGuid);
                return sqlHelper.ExecuteInt32(sql);
            }
        }

        public int GetCardholderId(Guid cardholderGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT ID FROM EMP WHERE OBJECT_GUID='{0}'", cardholderGuid);
                return sqlHelper.ExecuteInt32(sql);
            }
        }

        public int GetBadgeKey(Guid badgeGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT BADGEKEY FROM BADGE WHERE OBJECT_GUID='{0}'", badgeGuid);
                return sqlHelper.ExecuteInt32(sql);
            }
        }

        public List<Guid> GetAccessLevelGuidsForCardholder(Guid cardholderGuid)
        {
            var retVal = new List<Guid>();
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT ACCESS_LEVEL_GUID FROM EMP_ACCESSLVL_LINK WHERE EMP_GUID='{0}'", cardholderGuid);
                var table = sqlHelper.ExecuteDataTable(sql);
                foreach (DataRow row in table.Rows)
                {
                    retVal.Add(Guid.Parse(row[0].ToString()));
                }
            }
            return retVal;
        }

        public List<int> GetAccessLevelIdsForCardholder(Guid cardholderGuid)
        {
            var retVal = new List<int>();
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT ACCESSLVL.ACCESSLVID FROM EMP_ACCESSLVL_LINK JOIN ACCESSLVL ON ACCESSLVL.OBJECT_GUID=EMP_ACCESSLVL_LINK.ACCESS_LEVEL_GUID WHERE EMP_ACCESSLVL_LINK.EMP_GUID='{0}'", cardholderGuid);
                var table = sqlHelper.ExecuteDataTable(sql);
                foreach (DataRow row in table.Rows)
                {
                    retVal.Add(Convert.ToInt32(row[0]));
                }
            }
            return retVal;
        }

        public void UpdateCardholderAccessLevelAssignments(Guid cardholderGuid, List<Guid> accessLevelGuids)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("DELETE FROM EMP_ACCESSLVL_LINK WHERE EMP_GUID='{0}'", cardholderGuid);
                sqlHelper.ExecuteNonQuery(sql);
                accessLevelGuids.ForEach(g =>
                {
                    sql = string.Format("INSERT INTO EMP_ACCESSLVL_LINK (EMP_GUID, ACCESS_LEVEL_GUID) VALUES('{0}','{1}')", cardholderGuid, g);
                    sqlHelper.ExecuteNonQuery(sql);
                });

                //TODO: Use OpenAccess to update Lnl_AccessLevelAssignment
                var accessLevelIds = accessLevelGuids.Select(g => GetAccessLevelId(g)).Where(i => i > 0).ToList();
                var badgeKeys = GetBadgeKeysForCardholder(cardholderGuid);
                badgeKeys.ForEach(k => UpdateBadgeAccessLevelAssignments(k, accessLevelIds));
            }
        }

        public void DeleteCardholderAccessLevelAssignments(Guid cardholderGuid)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("DELETE FROM EMP_ACCESSLVL_LINK WHERE EMP_GUID='{0}'", cardholderGuid);
                sqlHelper.ExecuteNonQuery(sql);
            }
        }

        public void UpdateBadgeAccessLevelAssignments(int badgeKey, List<int> accessLevelIds)
        {
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("DELETE FROM BADGELINK WHERE BADGEKEY={0}", badgeKey);
                sqlHelper.ExecuteNonQuery(sql);
                accessLevelIds.ForEach(l =>
                {
                    sql = string.Format("INSERT INTO BADGELINK (BADGEKEY, ACCLVLID, INTRUSION_LEVEL, ASSIGNMENT_TYPE) VALUES({0},{1},0,0)", badgeKey, l);
                    sqlHelper.ExecuteNonQuery(sql);
                });
            }
        }

        private List<int> GetBadgeKeysForCardholder(Guid cardholderGuid)
        {
            var retVal = new List<int>();
            using (var sqlHelper = new SqlHelper())
            {
                var sql = string.Format("SELECT BADGEKEY FROM BADGE JOIN EMP ON EMP.ID=BADGE.EMPID WHERE EMP.OBJECT_GUID='{0}'", cardholderGuid);
                var table = sqlHelper.ExecuteDataTable(sql);
                foreach (DataRow row in table.Rows)
                {
                    retVal.Add(Convert.ToInt32(row[0]));
                }
            }
            return retVal;
        }

        private void InitializeDatabase()
        {
            if (isDatabaseInitialized) return;
            using (var sqlHelper = new SqlHelper())
            {
                var tableCreated = sqlHelper.ExecuteInt32("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME='EMP_ACCESSLVL_LINK'") > 0;
                if (!tableCreated)
                {
                    sqlHelper.ExecuteNonQuery("CREATE TABLE EMP_ACCESSLVL_LINK(EMP_GUID uniqueidentifier NOT NULL,ACCESS_LEVEL_GUID uniqueidentifier NOT NULL)");
                    sqlHelper.ExecuteNonQuery("ALTER TABLE ACCESSLVL ADD OBJECT_GUID uniqueidentifier NULL");
                    sqlHelper.ExecuteNonQuery("ALTER TABLE EMP ADD OBJECT_GUID uniqueidentifier NULL");
                    sqlHelper.ExecuteNonQuery("ALTER TABLE BADGE ADD OBJECT_GUID uniqueidentifier NULL");
                }
            }
            isDatabaseInitialized = true;
        }
    }

    public class SqlHelper : IDisposable
    {
        private SqlConnection connection;

        public SqlHelper()
        {
            var connectionString = AppConfigUtility.GetOnGuardDatabaseConnectionString();
            connection = new SqlConnection(connectionString);
            connection.Open();
        }

        public void ExecuteNonQuery(string commandText)
        {
            using (var command = new SqlCommand(commandText, connection))
            {
                command.ExecuteNonQuery();
            }
        }

        public int ExecuteInt32(string commandText)
        {
            int result = 0;

            using (var command = new SqlCommand(commandText, connection))
            {
                var o = command.ExecuteScalar();
                if (o != null)
                {
                    int.TryParse(o.ToString(), out result);
                }
            }

            return result;
        }

        public string ExecuteString(string commandText)
        {
            var result = string.Empty;

            using (var command = new SqlCommand(commandText, connection))
            {
                var o = command.ExecuteScalar();
                if (o != null)
                {
                    result = o.ToString();
                }
            }

            return result;
        }

        public bool ExecuteBoolean(string commandText)
        {
            bool result = false;

            using (var command = new SqlCommand(commandText, connection))
            {
                var o = command.ExecuteScalar();
                if (o != null)
                {
                    bool.TryParse(o.ToString(), out result);
                }
            }

            return result;
        }

        public Guid ExecuteGuid(string commandText)
        {
            var result = Guid.Empty;

            using (var command = new SqlCommand(commandText, connection))
            {
                var o = command.ExecuteScalar();
                if (o != null)
                {
                    Guid.TryParse(o.ToString(), out result);
                }
            }

            return result;
        }

        public DataTable ExecuteDataTable(string commandText)
        {
            var dataTable = new DataTable();

            using (var command = new SqlCommand(commandText, connection))
            {
                command.CommandTimeout = 600;
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
            }

            return dataTable;
        }

        public DataRow ExecuteDataRow(string commandText)
        {
            DataRow dataRow = null;

            using (var command = new SqlCommand(commandText, connection))
            {
                var dataTable = new DataTable();
                var adapter = new SqlDataAdapter(command);
                adapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    dataRow = dataTable.Rows[0];
                }
            }

            return dataRow;
        }

        void IDisposable.Dispose()
        {
            if (connection != null && connection.State != ConnectionState.Closed)
            {
                connection.Close();
            }
        }
    }
}