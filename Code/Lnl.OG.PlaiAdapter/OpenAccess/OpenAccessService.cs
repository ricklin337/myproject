﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// A proxy class that interact with Open Access service.
    /// </summary>
    public class OpenAccessService
    {
        #region Private Constants
        private const string EventSubscriptionsUri = "event_subscriptions";
        private const string AddOrModifyCardholderUri = "instances?type_name=Lnl_Cardholder";
        private const string DeleteCardholderUri = "instances?type_name=Lnl_Cardholder";
        private const string GetCardholderUri = "instances?type_name=Lnl_Cardholder&version=1.0&page_number=1&page_size=1&filter=ID%3D{0}";
        private const string AddOrModifyAccessLevelUri = "instances?type_name=Lnl_AccessLevel";
        private const string DeleteAccessLevelUri = "instances?type_name=Lnl_AccessLevel";
        private const string GetAccessLevelUri = "instances?type_name=Lnl_AccessLevel&version=1.0&page_number=1&page_size=1&filter=ID%3D{0}";
        private const string AddOrModifyBadgeUri = "instances?type_name=Lnl_Badge";
        private const string DeleteBadgeUri = "instances?type_name=Lnl_Badge";
        private const string GetBadgeUri = "instances?type_name=Lnl_Badge&version=1.0&page_number=1&page_size=1&filter=BADGEKEY%3D{0}";
        private const string AddOrDeleteAccessLevelAssignmentUri = "instances?type_name=Lnl_AccessLevelAssignment";
        private const string GetBadgeAccessLevelAssignmentUri = "instances?type_name=Lnl_AccessLevelAssignment&version=1.0&page_number=1&page_size=50&filter=BADGEKEY%3D{0}";
        #endregion
        
        #region Members
        private static string openAccessUrl;
        private static string applicationId;
        private static string sessionToken;
        public string ApplicationId { get { return applicationId; } }
        public string SessionToken { get { return sessionToken; } }
        private HttpClient client;
        private OnGuardDatabase onGuardDatabase = new OnGuardDatabase();
        #endregion

        /// <summary>
        /// Default Constructor
        /// </summary>
        public OpenAccessService()
        {
            if (string.IsNullOrEmpty(sessionToken))
            {
                openAccessUrl = AppConfigUtility.GetOpenAccessProxyUrl();
                applicationId = AppConfigUtility.GetOpenAccessApplicationId();
                ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, sslPolicyErrors) => true;
            }

            client = new HttpClient();
            client.BaseAddress = new Uri(openAccessUrl);
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            client.DefaultRequestHeaders.Add("application-id", applicationId);

            if (string.IsNullOrEmpty(sessionToken))
            {
                var userName = AppConfigUtility.GetOpenAccessUsername();
                var password = AppConfigUtility.GetOpenAccessPassword();
                var directoryId = AppConfigUtility.GetOpenAccessDirectoryId();
                Authenticate(userName, password, directoryId);
            }
            client.DefaultRequestHeaders.Add("session-token", sessionToken);
        }

        #region Public Methods
        /// <summary>
        /// Log out Open Access service.
        /// </summary>
        public void LogOut()
        {
            var request = new HttpRequestMessage(HttpMethod.Delete, "authentication");
            request.Content = new ObjectContent<DeleteAuthenticationRequest>(new DeleteAuthenticationRequest() { }, new JsonMediaTypeFormatter());
            var httpResponse = client.SendAsync(request).Result;
            ValidateSuccessResponse(httpResponse);

            // Seems no need to lock it since this method will be called once only when service shut down
            sessionToken = null;
        }

        /// <summary>
        /// Adds an event subscription
        /// </summary>
        /// <param name="subscription">The event subscription to add</param>
        /// <returns>The event subscription added</returns>
        public EventSubscription AddSubscription(EventSubscription subscription)
        {
            var httpResponse = client.PostAsJsonAsync(EventSubscriptionsUri, ToAddEventSubscriptionRequest(subscription)).Result;
            ValidateSuccessResponse(httpResponse);

            return httpResponse.Content.ReadAsAsync<EventSubscription>().Result;
        }

        /// <summary>
        /// Gets the cardholder based on provided cardholder guid.
        /// </summary>
        /// <param name="cardholderGuid">The cardholder guid</param>
        /// <returns>The cardholder information</returns>
        public Cardholder GetCardholder(Guid cardholderGuid)
        {
            var cardholderId = onGuardDatabase.GetCardholderId(cardholderGuid);
            var requestUri = string.Format(GetCardholderUri, cardholderId);
            var httpResponse = client.GetAsync(requestUri).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<GetInstancesResponse>().Result;

            if (response == null || response.item_list == null || response.item_list.Count <= 0 || response.item_list[0].property_value_map == null || response.item_list[0].property_value_map.Count <= 0)
                throw new PlaiAdapterException(HttpStatusCode.NotFound, "Cardholder Not Found!", string.Format("No Cardholder with GUID = {0}", cardholderGuid));

            return new Cardholder
            {
                ObjectGuid = cardholderGuid,
                FirstName = Convert.ToString(response.item_list[0].property_value_map["FIRSTNAME"]),
                LastName = Convert.ToString(response.item_list[0].property_value_map["LASTNAME"]),
                Email = Convert.ToString(response.item_list[0].property_value_map["EMAIL"]),
                AccessLevelGuids = onGuardDatabase.GetAccessLevelGuidsForCardholder(cardholderGuid)
            };
        }

        /// <summary>
        /// Creates a new cardholder if not found otherwise update
        /// </summary>
        /// <param name="cardholder">The cardholder to add or update</param>
        public void AddOrModifyCardholder(Cardholder cardholder)
        {
            if (onGuardDatabase.IsCardholderExist(cardholder.ObjectGuid))
                ModifyCardholder(cardholder);
            else
                AddCardholder(cardholder);
            onGuardDatabase.UpdateCardholderAccessLevelAssignments(cardholder.ObjectGuid, cardholder.AccessLevelGuids);
        }

        /// <summary>
        /// Deletes a cardholder based on provided cardholder guid.
        /// </summary>
        /// <param name="cardholderGuid">The cardholder guid to delete</param>
        public void DeleteCardholder(Guid cardholderGuid)
        {
            if (!onGuardDatabase.IsCardholderExist(cardholderGuid)) return;

            var request = new InstanceRequest { property_value_map = new Dictionary<string, object>() };
            request.property_value_map.Add("ID", onGuardDatabase.GetCardholderId(cardholderGuid));

            var message = new HttpRequestMessage(HttpMethod.Delete, DeleteCardholderUri);
            message.Content = new ObjectContent<InstanceRequest>(request, new JsonMediaTypeFormatter());

            var httpResponse = client.SendAsync(message).Result;
            ValidateSuccessResponse(httpResponse);

            onGuardDatabase.DeleteCardholderAccessLevelAssignments(cardholderGuid);
        }

        /// <summary>
        /// Gets an access level based on provided access level guid.
        /// </summary>
        /// <param name="accessLevelGuid">The access level guid</param>
        /// <returns></returns>
        public AccessLevel GetAccessLevel(Guid accessLevelGuid)
        {
            var accessLevelId = onGuardDatabase.GetAccessLevelId(accessLevelGuid);
            var requestUri = string.Format(GetAccessLevelUri, accessLevelId);
            var httpResponse = client.GetAsync(requestUri).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<GetInstancesResponse>().Result;

            if (response == null || response.item_list == null || response.item_list.Count <= 0 || response.item_list[0].property_value_map == null || response.item_list[0].property_value_map.Count <= 0)
                throw new PlaiAdapterException(HttpStatusCode.NotFound, "AccessLevel Not Found!", string.Format("No AccessLevel with GUID = {0}", accessLevelGuid));

            return new AccessLevel
            {
                ObjectGuid = accessLevelGuid,
                Name = Convert.ToString(response.item_list[0].property_value_map["Name"])
            };
        }

        /// <summary>
        /// Creates a new access level if not found otherwise update
        /// </summary>
        /// <param name="accessLevel">The access level to add or update</param>
        public void AddOrModifyAccessLevel(AccessLevel accessLevel)
        {
            if (onGuardDatabase.IsAccessLevelExist(accessLevel.ObjectGuid))
                ModifyAccessLevel(accessLevel);
            else
                AddAccessLevel(accessLevel);
        }

        /// <summary>
        /// Deletes a access level based on provided access level guid.
        /// </summary>
        /// <param name="accessLevelGuid">The access level guid to delete</param>
        public void DeleteAccessLevel(Guid accessLevelGuid)
        {
            if (!onGuardDatabase.IsAccessLevelExist(accessLevelGuid)) return;

            var request = new InstanceRequest { property_value_map = new Dictionary<string, object>() };
            request.property_value_map.Add("ID", onGuardDatabase.GetAccessLevelId(accessLevelGuid));

            var message = new HttpRequestMessage(HttpMethod.Delete, DeleteAccessLevelUri);
            message.Content = new ObjectContent<InstanceRequest>(request, new JsonMediaTypeFormatter());

            var httpResponse = client.SendAsync(message).Result;
            ValidateSuccessResponse(httpResponse);
        }

        /// <summary>
        /// Gets a badge based on provided badge guid.
        /// </summary>
        /// <param name="guid">The badge guid</param>
        /// <returns></returns>
        public Badge GetBadge(Guid guid)
        {
            var badgeKey = onGuardDatabase.GetBadgeKey(guid);
            var requestUri = string.Format(GetBadgeUri, badgeKey);
            var httpResponse = client.GetAsync(requestUri).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<GetInstancesResponse>().Result;

            if (response == null || response.item_list == null || response.item_list.Count <= 0 || response.item_list[0].property_value_map == null || response.item_list[0].property_value_map.Count <= 0)
                throw new PlaiAdapterException(HttpStatusCode.NotFound, "Badge Not Found!", string.Format("No Badge with GUID = {0}", guid));

            return new Badge
            {
                ObjectGuid = guid,
                Id = Convert.ToInt64(response.item_list[0].property_value_map["ID"]),
                PersonObjectGuid = onGuardDatabase.GetCardholderGuid(Convert.ToInt32(response.item_list[0].property_value_map["PERSONID"])),
                Status = (BadgeStatus)Convert.ToInt32(response.item_list[0].property_value_map["STATUS"]),
                Type = Convert.ToInt32(response.item_list[0].property_value_map["TYPE"]),
            };
        }

        /// <summary>
        /// Creates a badge if not found otherwise update
        /// </summary>
        /// <param name="badge">The badge to add or update</param>
        public void AddOrModifyBadge(Badge badge)
        {
            if (onGuardDatabase.IsBadgeExist(badge.ObjectGuid))
                ModifyBadge(badge);
            else
                AddBadge(badge);
        }

        /// <summary>
        /// Deletes a badge based on provided badge guid.
        /// </summary>
        /// <param name="badgeGuid">The badge guid to delete</param>
        public void DeleteBadge(Guid badgeGuid)
        {
            if (!onGuardDatabase.IsBadgeExist(badgeGuid)) return;

            var request = new InstanceRequest { property_value_map = new Dictionary<string, object>() };
            request.property_value_map.Add("BADGEKEY", onGuardDatabase.GetBadgeKey(badgeGuid));

            var message = new HttpRequestMessage(HttpMethod.Delete, DeleteBadgeUri);
            message.Content = new ObjectContent<InstanceRequest>(request, new JsonMediaTypeFormatter());

            var httpResponse = client.SendAsync(message).Result;
            ValidateSuccessResponse(httpResponse);
        }
        #endregion

        #region Private Methods
        /// <summary>
        /// Authenticates with the Open Access service
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <param name="directoryId"></param>
        private void Authenticate(string username, string password, string directoryId)
        {
            var request = new AddAuthenticationRequest { user_name = username, password = password, directory_id = directoryId };
            var httpResponse = client.PostAsJsonAsync("authentication", request).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<AddAuthenticationResponse>().Result;
            sessionToken = response.session_token;
        }

        /// <summary>
        /// Creates a new cardholder
        /// </summary>
        /// <param name="cardholder">The cardholder to add</param>
        private void AddCardholder(Cardholder cardholder)
        {
            var httpResponse = client.PostAsJsonAsync(AddOrModifyCardholderUri, ToInstanceRequest(cardholder, false)).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<AddInstanceResponse>().Result;
            var cardholderId = Convert.ToInt32(response.property_value_map["ID"]);
            onGuardDatabase.UpdateCardholderGuid(cardholderId, cardholder.ObjectGuid);
        }

        /// <summary>
        /// Updates an existing cardholder 
        /// </summary>
        /// <param name="cardholder">The cardholder to update</param>
        private void ModifyCardholder(Cardholder cardholder)
        {
            var httpResponse = client.PutAsJsonAsync(AddOrModifyCardholderUri, ToInstanceRequest(cardholder, true)).Result;
            ValidateSuccessResponse(httpResponse);
        }

        /// <summary>
        /// Creates a new access level
        /// </summary>
        /// <param name="accessLevel">The access level to add</param>
        private void AddAccessLevel(AccessLevel accessLevel)
        {
            var httpResponse = client.PostAsJsonAsync(AddOrModifyAccessLevelUri, ToInstanceRequest(accessLevel, false)).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<AddInstanceResponse>().Result;
            var accessLevelId = Convert.ToInt32(response.property_value_map["ID"]);
            onGuardDatabase.UpdateAccessLevelGuid(accessLevelId, accessLevel.ObjectGuid);
        }

        /// <summary>
        /// Updates an existing access level 
        /// </summary>
        /// <param name="accessLevel">The access level to update</param>
        private void ModifyAccessLevel(AccessLevel accessLevel)
        {
            var httpResponse = client.PutAsJsonAsync(AddOrModifyAccessLevelUri, ToInstanceRequest(accessLevel, true)).Result;
            ValidateSuccessResponse(httpResponse);
        }

        /// <summary>
        /// Creates a new badge
        /// </summary>
        /// <param name="badge">The badge to add</param>
        private void AddBadge(Badge badge)
        {
            if (!onGuardDatabase.IsCardholderExist(badge.PersonObjectGuid))
                throw new PlaiAdapterException(HttpStatusCode.BadRequest, "CredentialInfo.AssignedToID GUID Not Found!");

            var httpResponse = client.PostAsJsonAsync(AddOrModifyBadgeUri, ToInstanceRequest(badge, false)).Result;
            ValidateSuccessResponse(httpResponse);
            var response = httpResponse.Content.ReadAsAsync<AddInstanceResponse>().Result;
            var badgeKey = Convert.ToInt32(response.property_value_map["BADGEKEY"]);
            onGuardDatabase.UpdateBadgeGuid(badgeKey, badge.ObjectGuid);

            // Add access levels to this new badge
            // TODO: Use OpenAccess to do it.
            var accessLevelIds = onGuardDatabase.GetAccessLevelIdsForCardholder(badge.PersonObjectGuid);
            onGuardDatabase.UpdateBadgeAccessLevelAssignments(badgeKey, accessLevelIds);
        }

        /// <summary>
        /// Updates an existing badge 
        /// </summary>
        /// <param name="badge">The badge to update</param>
        private void ModifyBadge(Badge badge)
        {
            var httpResponse = client.PutAsJsonAsync(AddOrModifyBadgeUri, ToInstanceRequest(badge, true)).Result;
            ValidateSuccessResponse(httpResponse);
        }

        /// <summary>
        /// Converts an event subscription to an AddEventSubscriptionRequest
        /// </summary>
        /// <param name="subscription">The subscription used to build the request</param>
        /// <returns>The AddEventSubscriptionRequest</returns>
        private static AddEventSubscriptionRequest ToAddEventSubscriptionRequest(EventSubscription subscription)
        {
            return new AddEventSubscriptionRequest
            {
                description = subscription.description ?? string.Empty,
                filter = subscription.filter ?? string.Empty,
                is_durable = subscription.is_durable
            };
        }

        /// <summary>
        /// Converts a cardholder to an InstanceRequest
        /// </summary>
        /// <param name="cardholder">The cardholder used to build the request</param>
        /// <param name="isModify">True if it is modify operation, otherwise false.</param>
        /// <returns>The InstanceRequest</returns>
        private InstanceRequest ToInstanceRequest(Cardholder cardholder, bool isModify )
        {
            var request = new InstanceRequest { property_value_map = new Dictionary<string, object>() };
            request.property_value_map.Add("FIRSTNAME", cardholder.FirstName);
            request.property_value_map.Add("LASTNAME", cardholder.LastName);
            request.property_value_map.Add("EMAIL", cardholder.Email);
            if (isModify) request.property_value_map.Add("ID", onGuardDatabase.GetCardholderId(cardholder.ObjectGuid));
            return request;
        }

        /// <summary>
        /// Converts a access level to an InstanceRequest
        /// </summary>
        /// <param name="accessLevel">The access level used to build the request</param>
        /// <param name="isModify">True if it is modify operation, otherwise false.</param>
        /// <returns>The InstanceRequest</returns>
        private InstanceRequest ToInstanceRequest(AccessLevel accessLevel, bool isModify)
        {
            var request = new InstanceRequest { property_value_map = new Dictionary<string, object>() };
            request.property_value_map.Add("Name", accessLevel.Name);
            if (isModify) request.property_value_map.Add("ID", onGuardDatabase.GetAccessLevelId(accessLevel.ObjectGuid));
            return request;
        }

        /// <summary>
        /// Converts a badge to an InstanceRequest
        /// </summary>
        /// <param name="badge">The badge used to build the request</param>
        /// <param name="isModify">True if it is modify operation, otherwise false.</param>
        /// <returns>The InstanceRequest</returns>
        private InstanceRequest ToInstanceRequest(Badge badge, bool isModify)
        {
            var request = new InstanceRequest { property_value_map = new Dictionary<string, object>() };
            request.property_value_map.Add("ID", badge.Id);
            request.property_value_map.Add("STATUS", (int)badge.Status);
            request.property_value_map.Add("TYPE", badge.Type);
            if (isModify) request.property_value_map.Add("BADGEKEY", onGuardDatabase.GetBadgeKey(badge.ObjectGuid));
            if (!isModify) request.property_value_map.Add("PERSONID", onGuardDatabase.GetCardholderId(badge.PersonObjectGuid));
            return request;
        }
        
        /// <summary>
        /// Validates that an HTTP response does not represent an error.
        /// Throws a PlaiAdapterException if it's an error response.
        /// </summary>
        /// <param name="response"></param>
        private static void ValidateSuccessResponse(HttpResponseMessage response)
        {
            if (response.IsSuccessStatusCode) return;

            var errorResponse = response.Content.ReadAsAsync<ErrorResponse>().Result;
            throw new PlaiAdapterException(errorResponse.error.code, errorResponse.error.message);
        }
        #endregion
    }
}