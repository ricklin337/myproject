﻿using System.Collections.Concurrent;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    public class OpenAccessEventQueue
    {
        private static readonly OpenAccessEventQueue instance;
        public static OpenAccessEventQueue Instance() { return instance; }

        private ConcurrentQueue<OpenAccessEvent> queue;

        static OpenAccessEventQueue()
        {
            instance = new OpenAccessEventQueue();
        }

        private OpenAccessEventQueue()
        {
            queue = new ConcurrentQueue<OpenAccessEvent>();
        }

        public void EnQueue(OpenAccessEvent openAccessEvent)
        {
            //TODO: limit the queue size?

            queue.Enqueue(openAccessEvent);
        }

        public OpenAccessEvent Dequeue()
        {
            OpenAccessEvent retVal = null;
            if (queue.Count > 0 && queue.TryDequeue(out retVal)) return retVal;
            return null;
        }
    }
}