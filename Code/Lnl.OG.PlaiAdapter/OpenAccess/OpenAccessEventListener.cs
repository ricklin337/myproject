﻿using Microsoft.AspNet.SignalR.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// A class that allows a client to receive business events from Open Access Event Bridge.
    /// </summary>
    public class OpenAccessEventListener
    {
        private const string HubName = "Outbound";
        private const string EventFilter = "business_event_class eq 'software_event' and software_event_object_type eq 'Badge'";

        private OpenAccessService openAccessService;
        private HubConnection connection;
        private IHubProxy openAccessEventBridgeProxy;

        /// <summary>
        /// Creates an OpenAccessEventListener instance
        /// </summary>
        /// <param name="openAccessService">The open access service proxy</param>
        public OpenAccessEventListener(OpenAccessService openAccessService)
        {
            this.openAccessService = openAccessService;
        }

        /// <summary>
        /// Starts to receive the business events from Open Access Event Bridge.
        /// </summary>
        public void StartListening()
        {
            var subscription = new EventSubscription { description = "description", filter = EventFilter, is_durable = false };
            subscription = this.openAccessService.AddSubscription(subscription);

            var openAccessConnectionInfo = new ConnectionInfo
            {
                SessionToken = openAccessService.SessionToken,
                ApplicationId = openAccessService.ApplicationId
            };

            GetOpenAccessEventBridgeProxy().Invoke<EventSubscription>("ModifySubscription", openAccessConnectionInfo, subscription).ContinueWith(OnModifySubscription).Wait();
        }

        /// <summary>
        /// Stops listening business events
        /// </summary>
        public void StopListening()
        {
            GetOpenAccessEventBridgeProxy().Invoke("StopSubscription").ContinueWith(OnStopSubscription);
        }

        /// <summary>
        /// Handles the event when a subscription is modified
        /// </summary>
        /// <param name="task"></param>
        private void OnModifySubscription(Task<EventSubscription> task)
        {
            if (task.IsFaulted) throw task.Exception;
        }

        /// <summary>
        /// Handles the event when stop subscription is called
        /// </summary>
        /// <param name="task"></param>
        private void OnStopSubscription(Task task)
        {
            if (task.IsFaulted) Console.WriteLine(task.Exception); //TODO: Log the error.
        }

        protected IHubProxy GetOpenAccessEventBridgeProxy()
        {
            if (openAccessEventBridgeProxy == null)
            {
                var openAccessEventBridgeUri = AppConfigUtility.GetOpenAccessEventBridgeUrl();
                connection = new HubConnection(openAccessEventBridgeUri);

                openAccessEventBridgeProxy = connection.CreateHubProxy(HubName);
                openAccessEventBridgeProxy.On<IDictionary<string, object>>("OnBusinessEventReceived", OnBusinessEventReceived);
                openAccessEventBridgeProxy.On<string>("OnExceptionRaised", OnExceptionRaised);
                openAccessEventBridgeProxy.On("OnConnectionFromMessageBusLost", OnConnectionFromMessageBusLost);
                openAccessEventBridgeProxy.On("OnConnectionToMessageBusEstablished", OnConnectionToMessageBusEstablished);
                openAccessEventBridgeProxy.On<string>("OnManagementEvent", OnManagementEvent);

                connection.Start().ContinueWith(OnConnectionStart).Wait();
            }
            return openAccessEventBridgeProxy;
        }

        protected void OnManagementEvent(string message) { }

        /// <summary>
        /// Handles the event when a business event is received.
        /// </summary>
        /// <param name="eventProperties"></param>
        protected virtual void OnBusinessEventReceived(IDictionary<string, object> eventProperties)
        {
            OpenAccessEventQueue.Instance().EnQueue(new OpenAccessEvent(eventProperties));
        }

        protected virtual void OnExceptionRaised(string message) { } //TODO: log the error

        protected virtual void OnConnectionFromMessageBusLost() { } //TODO: re-connect

        protected virtual void OnConnectionToMessageBusEstablished() { }

        protected void OnConnectionStart(Task task)
        {
            if (task.IsFaulted) throw task.Exception;
        }
    }
}