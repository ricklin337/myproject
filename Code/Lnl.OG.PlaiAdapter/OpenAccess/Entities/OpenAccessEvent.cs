﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    public class OpenAccessEvent
    {
        private IDictionary<string, object> eventProperties;

        public OpenAccessEvent(IDictionary<string, object> eventProperties)
        {
            this.eventProperties = eventProperties;
        }

        private OpenAccessEventType? eventType;
        public OpenAccessEventType EventType
        {
            get
            {
                if (eventType == null)
                {
                    eventType = OpenAccessEventType.UnKnown;
                    if (eventProperties != null && eventProperties.ContainsKey("software_event_object_type") && eventProperties.ContainsKey("software_event_operation_type"))
                    {
                        if (eventProperties["software_event_object_type"].ToString() == "Badge")
                        {
                            if (eventProperties["software_event_operation_type"].ToString() == "Add")
                                eventType = OpenAccessEventType.AddBadge;
                            else if (eventProperties["software_event_operation_type"].ToString() == "Modify")
                                eventType = OpenAccessEventType.ModifyBadge;
                            else if (eventProperties["software_event_operation_type"].ToString() == "Delete")
                                eventType = OpenAccessEventType.DeleteBadge;
                        }
                    }
                }
                return eventType.Value;
            }
        }

        /// <summary>
        /// Gets the badge that was added from OnGuard. 
        /// Return null if this badge was added from PLAI
        /// Return null if this badge's cardholder does not come from PLAI.
        /// </summary>
        /// <returns></returns>
        public Badge GetBadgeFromAddBadgeEvent()
        {
            if (EventType != OpenAccessEventType.AddBadge) return null;

            // TODO: refine following code when OpenAccess support GUID.
            var onGuardDatabase = new OnGuardDatabase();
            var badgeKey = Convert.ToInt32(eventProperties["new_BADGEKEY"]);
            var cardholderId = Convert.ToInt32(eventProperties["new_PERSONID"]);
            var badgeGuid = onGuardDatabase.GetBadgeGuid(badgeKey);
            var cardholderGuid = onGuardDatabase.GetCardholderGuid(cardholderId);

            // If the cardholder guid does not exist, it means this cardholder was not added from PLAI or this cardholder does not exist
            if (cardholderGuid == Guid.Empty) return null;

            // If the badge guid exist, it means this badge was added from PLAI
            if (badgeGuid != Guid.Empty) return null;

            badgeGuid = Guid.NewGuid();
            onGuardDatabase.UpdateBadgeGuid(badgeKey, badgeGuid);

            // Update the BADGELINK
            var accessLevelIds = onGuardDatabase.GetAccessLevelIdsForCardholder(cardholderGuid);
            onGuardDatabase.UpdateBadgeAccessLevelAssignments(badgeKey, accessLevelIds);

            return new Badge
            {
                ObjectGuid = onGuardDatabase.GetBadgeGuid(badgeKey),
                PersonObjectGuid = onGuardDatabase.GetCardholderGuid(cardholderId),
                Id = Convert.ToInt64(eventProperties["new_ID"]),
                Status = (BadgeStatus)Convert.ToInt32(eventProperties["new_STATUS"]),
                Type = Convert.ToInt32(eventProperties["new_TYPE"]),
            };
        }

        public Badge GetBadgeFromModifyBadgeEvent() //TODO: new and old
        {
            if (EventType != OpenAccessEventType.ModifyBadge) return null;

            // TODO: refine following code when OpenAccess support GUID.
            var onGuardDatabase = new OnGuardDatabase();
            var badgeKey = Convert.ToInt32(eventProperties["new_BADGEKEY"]);
            var cardholderId = Convert.ToInt32(eventProperties["new_PERSONID"]);
            var badgeGuid = onGuardDatabase.GetBadgeGuid(badgeKey);
            var cardholderGuid = onGuardDatabase.GetCardholderGuid(cardholderId);
            
            if (cardholderGuid == Guid.Empty) return null;
            if (badgeGuid == Guid.Empty) return null;
            
            return new Badge
            {
                ObjectGuid = onGuardDatabase.GetBadgeGuid(badgeKey),
                PersonObjectGuid = onGuardDatabase.GetCardholderGuid(cardholderId),
                Id = Convert.ToInt64(eventProperties["new_ID"]),
                Status = (BadgeStatus)Convert.ToInt32(eventProperties["new_STATUS"]),
                Type = Convert.ToInt32(eventProperties["new_TYPE"]),
            };
        }
    }
}