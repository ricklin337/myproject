﻿namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Used to handle the authentication request
    /// </summary>
    public class AddAuthenticationRequest : BaseRequest
    {
        public string user_name;
        public string password;
        public string directory_id;
    }
}
