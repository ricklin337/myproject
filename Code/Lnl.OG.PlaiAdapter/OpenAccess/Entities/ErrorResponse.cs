﻿namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Represents an error response
    /// </summary>
    public class ErrorResponse
    {
        public Error error;
    }

    /// <summary>
    /// Open Access error
    /// </summary>
    public class Error
    {
        public string code;
        public string message;
    }
}
