﻿namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Base request for common properties.
    /// </summary>
    public class BaseRequest
    {
        public string version = "1.0";
    }
}