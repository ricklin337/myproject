﻿using System.Collections.Generic;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    public class InstanceItem
    {
        public Dictionary<string, object> property_value_map;
    }

    /// <summary>
    /// Represents the instances from the response of get_instances Open Access call.
    /// </summary>
    public class GetInstancesResponse
    {
        public int count;
        public List<InstanceItem> item_list;
        public int page_number;
        public int page_size;
        public int total_items;
        public int total_pages;
        public string type_name;
        public string version;
    }
}