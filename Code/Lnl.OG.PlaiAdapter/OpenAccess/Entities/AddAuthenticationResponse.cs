﻿using System;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Used to handle the authentication response
    /// </summary>
    public class AddAuthenticationResponse
    {
        public string session_token;
        public DateTime token_expiration_time;
    }
}
