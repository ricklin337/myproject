﻿namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Values representing the badge status
    /// </summary>
    public enum BadgeStatus
    {
        Active = 1,
        Lost = 2,
        Returned = 2,
    }
}
