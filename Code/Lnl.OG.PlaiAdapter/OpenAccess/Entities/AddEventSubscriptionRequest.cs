﻿namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Used to make an add event subscription request
    /// </summary>
    public class AddEventSubscriptionRequest : BaseRequest
    {
        public string description;
        public string filter;
        public bool is_durable;
    }
}
