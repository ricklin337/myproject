﻿namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// The info to connect to Open Access service.
    /// </summary>
    public class ConnectionInfo
    {
        public string SessionToken;
        public string ApplicationId;
    }
}
