﻿using System.Collections.Generic;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Represents the instance from the response of adding instance call.
    /// </summary>
    public class AddInstanceResponse
    {
        public Dictionary<string, object> property_value_map;
        public string type_name;
        public string version;
    }
}
