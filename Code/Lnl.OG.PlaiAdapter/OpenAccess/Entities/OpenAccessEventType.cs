﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Values representing the Open Access event type
    /// </summary>
    public enum OpenAccessEventType
    {
        UnKnown,
        AddBadge,
        ModifyBadge,
        DeleteBadge,
        AccessGranted,
        AccessDenied
    }
}