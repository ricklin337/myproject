﻿using System;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Entity class the represents an access level
    /// </summary>
    public class AccessLevel
    {
        public Guid ObjectGuid;
        public string Name;
    }
}
