﻿using System.Collections.Generic;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// The class is used to make an add/modify/delete instance request
    /// </summary>
    public class InstanceRequest : BaseRequest
    {
        public Dictionary<string, object> property_value_map;
    }
}
