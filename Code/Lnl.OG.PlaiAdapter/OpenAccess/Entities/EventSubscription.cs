﻿using System;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Represents an event subscription.
    /// </summary>
    public class EventSubscription
    {
        public int id;
        public string user_id;
        public string description;
        public string filter;
        public bool is_durable;
        public string queue_name;
        public string exchange_name;
        public string binding_key;
        public string message_broker_hostname;
        public int message_broker_port;
        public bool requires_secure_connection;
        public DateTime? created_date;
        public DateTime? last_updated_date;
    }
}
