﻿using System;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Entity class the represents a badge
    /// </summary>
    public class Badge
    {
        public Guid ObjectGuid;
        public Guid PersonObjectGuid;
        public long Id;
        public BadgeStatus Status;
        public int Type;
    }
}