﻿using System;
using System.Collections.Generic;

namespace Lnl.OG.PlaiAdapter.OpenAccess
{
    /// <summary>
    /// Entity class the represents a card holder
    /// </summary>
    public class Cardholder
    {
        public Guid ObjectGuid;
        public string FirstName;
        public string LastName;
        public string Email;
        public List<Guid> AccessLevelGuids;

        // TODO: add other optional attributes
    }
}